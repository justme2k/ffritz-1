ATOM_MODFILES = $(shell find $(PLAT_BASE)/atom/mod -type f -o -type d)

ATOM_ROOTIMG = $(PLAT_TMP)/uimage/part_03_ATOM_ROOTFS.bin

ATOM_PATCHES = usb-maxdevcount.patch

ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -lt 39 ; echo $$?),0)
ATOM_PATCHES += profile.patch
else
ATOM_PATCHES += profile-v1.patch
endif

ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -lt 19 ; echo $$?),0)
ATOM_PATCHES += 50-udev-default.patch
ATOM_PATCHES += hotplug-remap-v1.patch
else # >= 7.19
ATOM_PATCHES += 10-console.rules.patch
ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -lt 39 ; echo $$?),0)
ATOM_PATCHES += 20-rc-net-ffmultid.patch
endif

ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -lt 24 ; echo $$?),0)
ATOM_PATCHES += hotplug-remap-v2.patch
else # >= 7.24

ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -lt 39 ; echo $$?),0)
ATOM_PATCHES += hotplug-remap-v3.patch
else # >= 7.39
ATOM_PATCHES += hotplug-remap-v4.patch
endif

endif

endif

ATOM_PATCHST=$(ATOM_PATCHES:%=$(ATOM_TMP)/.applied.%)

$(ATOM_ROOTIMG): $(PLAT_TMP)/uimage


export PATH=$PATH:/usr/local/bin
export TERMINFO=/usr/local/lib/terminfo
test -d /usr/local/lib/perl5/5* && export PERLLIB=/usr/local/lib/perl5/5*

if [ -x /usr/local/bin/vim ]; then
  export EDITOR='/usr/local/bin/vim'
fi


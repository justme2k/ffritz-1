## Additional modules in package/x86 to build and install
#
MODULES   += nfs
MODULES   += bt
MODULES   += prtg
MODULES   += modules
MODULES   += cash
MODULES   += ffad

# Options for pihole. Requires BR_VERSION other than 2019-05 in toplevel conf.mk
ifneq ($(BR_VERSION),-2019.05)
MODULES   += ftl
MODULES   += libmultid
MODULES   += pi-hole
endif

ifneq ($(BR_VERSION_BASE),-2022.02)
MODULES   += unfs
endif

ifneq ($(BR_VERSION),-2022.02_glibc230)
MODULES   += wireguard-tools
endif
